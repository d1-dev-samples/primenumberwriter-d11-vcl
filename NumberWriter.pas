unit NumberWriter;

interface
uses
  SysUtils,
  SyncObjs;

type
  TNumberWriter = class
  public
    Constructor Create(Filename: string);
    destructor Destroy; override;

    function writeNumber(num: Integer): Boolean;
    function writeNumberSync(num: Integer): Boolean;
    function getLastWrittenNumber: integer;

    function isBusy: boolean;

  private
    cs: TCriticalSection;
    tf: TextFile;
    lastWrittenNumber: integer;
    busy: Boolean;
  end;

implementation

{ TNumberWriter }

constructor TNumberWriter.Create(Filename: string);
begin
  AssignFile(tf, Filename);
  cs := TCriticalSection.Create;
  lastWrittenNumber := 0;
  busy := False;
  try
    rewrite(tf);
  except
    //todo: do logging if needed
  end;
end;


destructor TNumberWriter.Destroy;
begin
  try
    CloseFile(tf);
  except
    //todo: do logging if needed
  end;
  cs.Free;
end;


//
// We don't have to use critical section for the next two methods because:
//   - we don't modify state
//   - integers and booleans are atomic
function TNumberWriter.getLastWrittenNumber: integer;
begin
  Result := lastWrittenNumber;
end;

function TNumberWriter.isBusy: boolean;
begin
  result := busy;
end;



function TNumberWriter.writeNumber(num: Integer): Boolean;
begin
  Result := false;
  if num > lastWrittenNumber then begin
    lastWrittenNumber := num;
    try
      write(tf, num, ' ');
      Result := true;
    except
      //todo: do logging if needed
    end;
  end;
end;


function TNumberWriter.writeNumberSync(num: Integer): Boolean;
begin
  Result := false;
  if num > lastWrittenNumber then begin
    cs.Enter;
    busy := True;
    try
      Result := writeNumber(num);
    finally
      busy := False;
      cs.Leave;
    end;
  end;
end;

end.
